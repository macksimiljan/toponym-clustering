# toponym-clustering
Clustering of toponyms by using a lanuage model.

Project at the seminar "Anwendungen Linguistischer Informatik", Uni Leipzig.

The final presentation is available [here](https://gitlab.com/macksimiljan/toponym-clustering/blob/master/pres_toponymClustering.pdf).
